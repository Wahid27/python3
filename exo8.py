students = [
  {
        "name": "gavi",
        "note": 12
    },
    {
        "name": "pedri",
        "note": 15
    },
    {
        "name": "ikay",
        "note": 7
    },
    {
        "name": "jules",
        "note": 9
    }
]
note_maximale = 0 
nom_meilleur_student = ""

for student in students:
    note = student["note"]
    if note > note_maximale:
        note_maximale = note
        nom_meilleur_student = student["name"]

print("L'élève avec la meilleure note est :", nom_meilleur_student)
print("Sa note est de :", note_maximale)
