

liste_de_nombres = [12, 45, 78, 34, 9, 23, 56, 89, 7, 15]

#le min
maximum = max(liste_de_nombres)

#le max
minimum = min(liste_de_nombres)

#  la moyenne
somme = sum(liste_de_nombres)
moyenne = somme / len(liste_de_nombres)

# Affichez les résultats
print("Liste de nombres :", liste_de_nombres)
print("Maximum :", maximum)
print("Minimum :", minimum)
print("Moyenne :", moyenne)
