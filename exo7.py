with open("text.txt", "r") as source_file:
    texte = source_file.read()

mots = texte.split()

nombre_de_mots = len(mots)

with open("resultat.txt", "w") as result_file:
    result_file.write(f"Le nombre de mots dans le fichier est : {nombre_de_mots}")

print("Le nombre de mots a été compté et écrit dans le fichier 'resultat.txt'.")
